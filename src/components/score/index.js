import React from 'react';
import './score.css'
const Score = props => {
const {wordScoreList, totalScore} = props
const words = Object.keys(wordScoreList)
const scores = Object.values(wordScoreList)

const wordList = words.map(function(word, index){
  return (<li key={index}>{word}</li>)
})

const scoreList = scores.map(function(score, index){
  return (<li key={index}>{score}</li>)
})

  return (
    <div>
        <div className="word-list">
            <div className="words">
            <h2>WORD</h2>
                {wordList}
            </div>
            <div className="scores">
            <h2>SCORE</h2>
                {scoreList}
            </div>
        </div>
        <div className="total-score">
            <h2>TOTAL SCORE</h2>
            <span>{totalScore}</span>
        </div>
    </div>
  );
};

export default Score;
