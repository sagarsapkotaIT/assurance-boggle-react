import React, { Component } from 'react';

import Board from '../board';
import {
    createBoard,
    copyBoard
} from '../../util/gameUtil';
import './game.css';
import Score from '../score';
class Game extends Component {
    constructor(props){
        super(props)
        this.board = [];
        this.scoreList = {};
        this.allBoardWords = [];
        this.state = {
            board: this.board,
            currentWord: '',
            isLoaded: false,
            score: 0,
            scoreList: this.scoreList,
            boardId : '',
            errorMessage: '',
            timerSeconds: 0,
            allBoardWords: this.allBoardWords
        }
    }

    getTimer(){
        var startTime = new Date().getTime();
        console.log(startTime);
        var that = this;
        var x = setInterval(function() {
            var now = new Date().getTime();
            var distance = now - startTime;

            // Time calculations for days, hours, minutes and seconds
            var timerSeconds = Math.floor(distance / 1000);
            console.log(timerSeconds);
            that.setState({
                timerSeconds: timerSeconds
            });
            console.log(that.state.timerSeconds);

            // If the count down is finished, write some text
            if (timerSeconds >= 120) {
                clearInterval(x);
                that.submitGame();
            }
        }, 1000);
    }

    submitGame(){
        const totalScore = this.state.score;
        const boardId = this.state.boardId;
        console.log(totalScore);
        console.log(boardId);
        this.setState({
            isLoaded: false
        });
        fetch("http://localhost:8080/board/submitGame/"+boardId+"/"+totalScore)
            .then(res => res.json())
            .then(
                (result) => {
                    if(result.success){

                        this.setState({
                            allBoardWords: result.params.response.boardAllWords,
                            timerSeconds: 0,
                            errorMessage: '',
                            isLoaded: true,
                            currentWord: '',
                            board: []
                        })
                        window.scrollTo(0, 0);
                    }else{
                        this.setState({
                            errorMessage: result.errorMessage
                        });
                    }
                },
                // Note: it's important to handle errors here
                // instead of a catch() block so that we don't swallow
                // exceptions from actual bugs in components.
                (error) => {
                    console.log(error);
                this.setState({
                    isLoaded: true,
                    error
                });
                }
            )
            this.clearBoard();
    }

    submitWord() {
        const currentWord = this.state.currentWord;
        const boardId = this.state.boardId;
        this.setState({
            isLoaded: false
        });
        fetch("http://localhost:8080/board/submitWord/"+boardId+"/"+currentWord)
            .then(res => res.json())
            .then(
                (result) => {
                    if(result.success){
                        this.state.scoreList[currentWord]=result.params.score
                        this.setState({
                            isLoaded: true,
                            score: this.state.score+result.params.score,
                            errorMessage: ''
                        });
                    }else{
                        this.setState({
                            errorMessage: result.errorMessage
                        });
                    }
                },
                // Note: it's important to handle errors here
                // instead of a catch() block so that we don't swallow
                // exceptions from actual bugs in components.
                (error) => {
                    console.log(error);
                this.setState({
                    isLoaded: true,
                    error
                });
                }
            )
            this.setState({
                currentWord: ''
            });
            this.clearBoard();
    }

    clearBoard(){
        this.state.board.forEach(getRow);
        function getRow(arr, row){
            arr.forEach(getCol);
            function getCol(cell, col){
                console.log(cell);
                cell.selected=false;
            }
        }  
    }

    // cell Clicked
    handleClick(rowId, columnId) {
        const letter = this.state.board[rowId][columnId].letter

        const newBoard = copyBoard(this.state.board)
        const selected = newBoard[rowId][columnId].selected
        console.log(selected);
        newBoard[rowId][columnId].selected = !selected;

        let newWord;
        if(!selected){
            newWord = this.state.currentWord.concat(letter)
        }else {
            newWord = this.state.currentWord.slice(0, -1)
        }
        
        this.setState({currentWord: newWord, board: newBoard})
    }

    componentDidMount() {
        fetch("http://localhost:8080/board/getNewBoard")
          .then(res => res.json())
          .then(
            (result) => {
                console.log(result.params.board);
                if(result.success){
                    this.setState({
                        isLoaded: true,
                        board: createBoard(result.params.board),
                        boardId : result.params.boardId,
                        errorMessage: ''
                    });
                    this.getTimer();
                }else{
                    this.setState({
                        errorMessage: result.errorMessage
                    });
                }

            },
            // Note: it's important to handle errors here
            // instead of a catch() block so that we don't swallow
            // exceptions from actual bugs in components.
            (error) => {
              this.setState({
                isLoaded: true,
                error
              });
            }
          )
      }


      refreshPage() {
        window.location.reload(false);
      }
      
    
    render(){
        return (
            <div>
                <div>
                    <h3 className={this.state.allBoardWords.length!==0?'allWordHeading':'hidden'}>Game is Over! Find Board All Words Below </h3> 
                    <p className="allBoardWordList">{this.state.allBoardWords.join(", ")}</p>
                    <button onClick={this.refreshPage} className={this.state.allBoardWords.length!==0?'startNewGame':'hidden'} > Start New Game </button>
                </div>
                <div>
                    {/* Display the countdown timer in an element */}  
                    <p id="display-timer">{this.state.timerSeconds} Seconds remaining</p>
                </div>
                <div className="errorMessage">{this.state.errorMessage!==''?this.state.errorMessage:""}</div>
                <div className="game-area">
                    <Board 
                        board={this.state.board}
                        handleClick={(rowId, columnId)=>this.handleClick(rowId, columnId)}
                    />
                    <div className="word-area">
                        <div className="current-header">
                            Current Word : 
                        </div>
                        <div className="current-word">
                            {this.state.currentWord}
                        </div>
                    </div>
                    <button className="button" onClick={()=> this.submitWord() } >
                        Submit
                    </button>
                </div>
                <div className="score-box">
                    <Score
                        wordScoreList={this.state.scoreList}
                        totalScore = {this.state.score}
                    />
                </div>
                <div className="clear" />
            </div>
        )
    }
}

export default Game;