import React from 'react';
import Cell from './cell';
import './board.css';

const Board = props => {
  const { board, handleClick } = props;
  return (
    <div className="boggle-board">
      {board.map((row, index) => {
        return (
          <div className="row" key={index}>
            {row.map((cell, index) => {
              return (
                <Cell
                  letter={cell.letter}
                  columnId={cell.columnId}
                  rowId={cell.row}
                  selected={cell.selected}
                  key={index}
                  handleClick={()=>handleClick(cell.rowId, cell.columnId)}
                />
              );
            })}
          </div>
        );
      })}
    </div>
  );
};

export default Board;
