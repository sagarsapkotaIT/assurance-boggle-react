export default class CellData {
    constructor(letter, rowId, columnId, selected = false) {
      this.letter = letter;
      this.rowId = rowId;
      this.columnId = columnId;
      this.selected = selected;
    }
  
    // Returns a new TileData instance with the same properties
    clone() {
      return new CellData(this.letter, this.rowId, this.columnId, this.selected)
    }
  }
  