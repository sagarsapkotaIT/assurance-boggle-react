import React from 'react';
import './cell.css';

const Cell = props => {
  const {letter, selected, handleClick} = props
  
  return (
    <button
      className={selected ? 'cell-selected' : 'cell'}
      onClick={()=>handleClick()}
    >
      {letter}
    </button>
  );
};

export default Cell;
