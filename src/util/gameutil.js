import CellData from "../components/board/cell/cellData";

export const createBoard = letterBoard => {
    const board = [];
    letterBoard.forEach(createRow);
    function createRow(arr, row){
        const rowData = [];
        arr.split("").forEach(createColumn);
        function createColumn(char, col){
            const cellData = new CellData(char, row, col);
            rowData.push(cellData);
        }
        board.push(rowData);
    }
    return board;
}

export const copyBoard = board => {
    const copiedBoard = board.map(row => {
      return row.map(cell => {
        return cell.clone();
      });
    });
    return copiedBoard;
  };