import React, { Component } from 'react';
import logo from './logo.jpg';
import './App.css';
import Game from './components/game';

class  App extends Component {
  render (){
    return (
    <div className="container">
      <header className="header">
        <img src={logo} className="boggle-logo" alt="logo" />
      </header>
      <Game />
    </div>
    )
  };
}

export default App;
